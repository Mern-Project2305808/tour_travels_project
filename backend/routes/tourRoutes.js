const express = require('express');
const router = express.Router();
const Tour = require('../models/Tour');

router.post('/', async (req, res) => {
    try {
        console.log('Request body:', req.body);
        const newTour = new Tour(req.body);
        const savedTour = await newTour.save();
        res.status(201).json(savedTour);
    } catch (err) {
        console.error('Error:', err);
        res.status(500).json({ message: err.message });
    }
});

module.exports = router;
