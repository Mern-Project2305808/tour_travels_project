const express = require('express')
const router = express.Router();
const mongodb = require('mongodb').MongoClient;

module.exports=router.get('/',(req,res)=>{
    mongodb.connect('mongodb://localhost:27017/tour',(err,db) => {
        if(err){
            console.error("mongodb connection error")
            return res.send("server is error")
        }
        else{
            db.collection('tourdetails').find().toArray((err,data) => {
                if(err){
                    console.error("while fetching data from collection",err)
                }
                else{
                    if(data.length > 0){
                        res.status(200).json(data)
                    }
                    else{
                        res.status(400).send("user not found")
                    }
                }
            })
        }
    })
})