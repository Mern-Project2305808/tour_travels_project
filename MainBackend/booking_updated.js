const express = require('express')
const router = express.Router();
const mongodb = require('mongodb').MongoClient;
const {ObjectId} = require('mongodb')

module.exports = router.put('/:id' , (req, res) => {
    const bookingId = req.params.id;
    const data = req.body;

    mongodb.connect('mongodb://localhost:27017/tour', (err, db) => {
        if(err) {
            console.error("mongodb connection error")
            return res.status(404).send("internal server error")
        }
        else {
            db.collection('booking').updateOne({_id: new ObjectId(bookingId)}, {$set: data} ,(err, result) => {
                if(err) {
                    console.error("data collection error")
                    return res.status(400).send("internal server error")
                }
                else{
                    if(result.matchedCount === 0 ){
                        return res.status(500).send("data not found")
                    }
                    else{
                        return res.status(201).send("data updated")
                    }
                }
            } )
        }
    })
})
