const express = require('express');
const { MongoClient, ObjectId } = require('mongodb');
const router = express.Router();

async function updateReviewAndLinkToTour(db, reviewId, data) {
    try {
        console.log('Updating review:', reviewId, 'with data:', data);

        // Insert or update the review
        const result = await db.collection('reviews').updateOne(
            { _id: new ObjectId(reviewId) },
            { $set: data },
            { upsert: true }
        );

        if (result.upsertedCount > 0) {
            console.log('Inserted new review with ID:', result.upsertedId._id);
        } else if (result.matchedCount > 0) {
            console.log('Updated existing review with ID:', reviewId);
        } else {
            console.log('No review found or updated with ID:', reviewId);
        }

        // Fetch the updated review
        const updatedReview = await db.collection('reviews').findOne({ _id: new ObjectId(reviewId) });
        console.log('Updated review:', updatedReview);

        if (updatedReview && updatedReview.productId) {
            console.log('Linking review to tour ID:', updatedReview.productId);
            // Ensure the tour document has this review in its reviews array
            await db.collection('tourdetails').updateOne(
                { _id: new ObjectId(updatedReview.productId) },
                { $addToSet: { reviews: new ObjectId(reviewId) } }
            );
            console.log('Successfully linked review to tour.');
        } else {
            console.log('No productId found in updated review.');
        }

        return { status: 200, message: "Review updated successfully", result };
    } catch (err) {
        console.error("Error updating review", err);
        throw err;
    }
}

router.post('/:id', async (req, res) => {
    const reviewId = req.params.id;
    const data = req.body;

    try {
        const client = await MongoClient.connect('mongodb://localhost:27017', { useNewUrlParser: true, useUnifiedTopology: true });
        const db = client.db("tour_travel");

        const result = await updateReviewAndLinkToTour(db, reviewId, data);
        res.status(result.status).json(result);

        client.close(); // Close the database connection
    } catch (err) {
        res.status(500).send("Internal server error");
    }
});

module.exports = router;
