// const express = require('express')
// const router =express.Router();
// const mongodb = require('mongodb').MongoClient;

// module.exports = router.post('/', (req,res) => {
//     const data = {
        
//         "mail":(req.body.mail),
//         "password":(req.body.password)
//     }

//     mongodb.connect('mongodb://localhost:27017/tour',(err,db) =>{
//         if(err){
//             console.error("mongodb connection error")
//             return res.status(404).send("server is error")  
//         }
//         else{
//             db.collection('tourdetails').insertOne( data, (err, result) =>{
//                 if(err){
//                     console.error("collection error")
//                 }
//                 else{
//                     res.status(201).send("successfully inserted")
//                 }
//             })
//         }
//     })
// })

const express = require('express')
const router =express.Router();
const mongodb = require('mongodb').MongoClient;
const bcrypt = require('bcrypt');//for hashing passwords

module.exports = router.post('/', async(req,res) => {
    const { mail, password } =req.body;

    try {
        const client = await mongodb.connect('mongodb://localhost:27017/tour' );
        const db = client.db('Registration');
        const user = await db.collection('users').findOne({ mail: mail});

        if (!user){
            res.status(400).send("user is not registered");
        }

        const isPasswordMatch =  await bcrypt.compare(password , user.password);
        if(!isPasswordMatch){
            return res.status(400).send("password Invalid");
        }

        res.status(200).send("Login Successfully");
        client.close();
    }catch (err) {
        console.error("Error during login" , err);
        res.status(500).send("Internal server error")
    }
})
