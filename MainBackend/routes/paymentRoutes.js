const express = require('express');
const router = express.Router();
const { client } = require('../config/paypal');

// Create Order
router.post('/create-order', async (req, res) => {
    const { amount } = req.body;

    const request = new paypal.orders.OrdersCreateRequest();
    request.prefer("return=representation");
    request.requestBody({
        intent: 'CAPTURE',
        purchase_units: [{
            amount: {
                currency_code: 'USD',
                value: amount
            }
        }]
    });

    try {
        const order = await client.execute(request);
        res.status(200).json({
            id: order.result.id
        });
    } catch (error) {
        console.error('Error creating PayPal order:', error);
        res.status(500).json({ message: error.message });
    }
});

// Capture Order
router.post('/capture-order', async (req, res) => {
    const { orderID } = req.body;

    const request = new paypal.orders.OrdersCaptureRequest(orderID);
    request.requestBody({});

    try {
        const capture = await client.execute(request);
        res.status(200).json({
            status: capture.result.status
        });
    } catch (error) {
        console.error('Error capturing PayPal order:', error);
        res.status(500).json({ message: error.message });
    }
});

module.exports = router;
