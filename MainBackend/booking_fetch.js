const express = require('express')
const router = express.Router();
const mongodb = require('mongodb').MongoClient;

module.exports = router.get('/' ,async (req, res) => {
    await mongodb.connect('mongodb://localhost:27017/tour',async (err, db) => {
        if(err) {
            console.error("mongodb connection error")
            return res.status(400).send("internal server error")
        }
        else{
            await db.collection('booking').find().toArray((err, data) => {
                if(err) {
                    console.error("while fetching data error")
                    return res.status(401).send("internal server error")
                }
                else{
                    if(data.length === 0){
                        console.error("data not found")
                        return res.status(500).send("data not found")
                    }
                    else{
                        res.status(201).json(data)
                    }
                }
            })
        }
    })
})
