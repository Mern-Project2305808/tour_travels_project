const express = require('express')
const router = express.Router();
const mongodb = require('mongodb').MongoClient;
module.exports=router.post('/',(req,res)=>{
    const data = req.body;

    mongodb.connect('mongodb://localhost:27017/tour',(err, db, client) => {
        if(err){
            console.error("Error connecting to MongoDB", err);
            return res.status(500).send("Internal server Error");
        }
        db.collection('tourdetails').insert(data, (err, result) => {
            if(err){
                console.error("Error inserting data into collection", err);
                return res.status(500).send("Internal server error");
            }
            res.status(201).json(result)
        })

    })
})