const express = require('express');
const router = express.Router();
const { MongoClient, ObjectId } = require('mongodb');

// Use the correct syntax for the post route
router.post('/:id', async (req, res) => {
    const reviewId = req.params.id;
    const data = req.body;

    try {
        const client = await MongoClient.connect('mongodb://localhost:27017');
        const db = client.db("tour_travel");

        const result = await db.collection('reviews').insertOne({ _id: new ObjectId(reviewId), ...data });
        res.status(201).json(result);

        client.close(); // Close the database connection
    } catch (err) {
        console.error("Error inserting data into collection", err);
        res.status(500).send("Internal server error");
    }
});

module.exports = router;
