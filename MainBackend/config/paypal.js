const paypal = require('@paypal/checkout-server-sdk');

const PAYPAL_CLIENT_ID = 'your_paypal_client_id';
const PAYPAL_CLIENT_SECRET = 'your_paypal_client_secret';

let environment = new paypal.core.SandboxEnvironment(PAYPAL_CLIENT_ID, PAYPAL_CLIENT_SECRET);
let client = new paypal.core.PayPalHttpClient(environment);

module.exports = { client };
