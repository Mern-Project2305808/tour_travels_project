// const express = require('express')
// const router =express.Router();
// const mongodb = require('mongodb').MongoClient;

// module.exports = router.post('/', (req,res) => {
//     const data = {
//         "username":(req.body.username),
//         "mail":(req.body.mail),
//         "password":(req.body.password)
//     }

//     mongodb.connect('mongodb://localhost:27017/tour',(err,db) =>{
//         if(err){
//             console.error("mongodb connection error")
//             return res.status(404).send("server is error")  
//         }
//         else{
//             db.collection('tourdetails').insertOne( data, (err, result) =>{
//                 if(err){
//                     console.error("collection error")
//                 }
//                 else{
//                     res.status(201).send("successfully inserted")
//                 }
//             })
//         }
//     })
// })





// // const express = require('express');
// // const router = express.Router();
// // const mongodb = require('mongodb').MongoClient;

// // router.post('/', (req, res) => {
// //     const data = {
// //         username: req.body.username,
// //         mail: req.body.mail,
// //         password: req.body.password
// //     };

// //     mongodb.connect('mongodb://localhost:27017/tour_travel', (err, client) => {
// //         if (err) {
// //             console.error("mongodb connection error");
// //             return res.status(500).send("Server error");
// //         }

// //         const db = client.db('tour_travel'); // Correct way to get the database
// //         db.collection('tourdetails').insertOne(data, (err, result) => {
// //             if (err) {
// //                 console.error("collection error");
// //                 return res.status(500).send("Error inserting data");
// //             } else {
// //                 res.status(201).send("Successfully inserted");
// //             }
// //         });
// //     });
// // });

// // module.exports = router;

const express = require('express');
const router = express.Router();
const mongodb = require('mongodb').MongoClient;
const bcrypt = require('bcrypt');

module.exports = router.post('/', async (req, res) => {
    const { username, mail, password } = req.body;

    try {
        // Connect to MongoDB
        const client = await mongodb.connect('mongodb://localhost:27017');
        const db = client.db('Registration');

        // Check if the user already exists
        const userExists = await db.collection('users').findOne({ mail: mail });
        if (userExists) {
            res.status(400).send("User already exists");
            client.close();
            return;
        }

        // Hash the password
        const hashedPassword = await bcrypt.hash(password, 10);

        // Save the user to the database
        await db.collection('users').insertOne({ username: username, mail: mail, password: hashedPassword });

        res.status(201).send("User successfully registered");
        client.close();
    } catch (err) {
        console.error("Error during registration", err);
        res.status(500).send("Internal server error");
    }
});



