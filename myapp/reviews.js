// const express = require('express')
// const router = express.Router();
// const mongodb = require('mongodb').MongoClient;

// module.exports = router.put('/:id', async (req, res) => {
//     const reviewId = req.params.id;
//     const updatedData = req.body;

//     try {
//         const db = await mongodb.connect('mongodb://localhost:27017/tour_travel');
       
//         //update the review
//         const reviewResult = await db.collection('reviews').updateOne({ _id: new mongodb.objectID(reviewId)},{$set: updatedData}); 
//         if(reviewResult.matchedCount === 0){
//             return res.status(404).send("Review not found")
//         }

//         // Optionally, update the corresponding tour's reviews array if needed
//         // If the tourId is part of the review document, you might need to do something like this:
//         const updatedReview = await db.collection('reviews').findOne({ _id: new mongodb.objectID(reviewId)})
//         const tourId = updatedReview.tourId;

//         if(tourId) {
//             // /Ensure the tour document has this review in its reviews array
//             await db.collection('tourdetails').updateOne({_id: new mongodb.objectID(tourId)},{$addToSet: {reviews: reviewId }})
//         }

//         res.status(200).send("review updated successfully");
//     } catch (err) {
//         console.error("error updating review",err);
//         res.status(500).send("internal server error");
//     }
// })




// const express = require('express');
// const router = express.Router();
// const mongodb = require('mongodb').MongoClient;

// // Update a review
// router.put('/:id', async (req, res) => {  // Corrected here
//     const reviewId = req.params.id;
//     const updatedData = req.body;

//     try {
//         const client = await mongodb.connect('mongodb://localhost:27017');
//         const db = client.db("tour_travel");
    

//         // Update the review
//         const reviewResult = await db.collection('reviews').updateOne(
//             { _id: new mongodb.ObjectID(reviewId) },
//             { $set: updatedData }
//         );

//         if (reviewResult.matchedCount === 0) {
//             return res.status(404).send("Review not found");
//         }

//         // Retrieve the updated review to get the tourId
//         const updatedReview = await db.collection('reviews').findOne({ _id: new mongodb.ObjectID(reviewId) });
//         const tourId = updatedReview.tourId;

//         if (tourId) {
//             // Ensure the tour document has this review in its reviews array
//             await db.collection('tourdetails').updateOne(
//                 { _id: new mongodb.ObjectID(tourId) },
//                 { $addToSet: { reviews: reviewId } }
//             );
//         }

//         res.status(200).send("Review updated successfully");
//         client.close();  // Close the database connection
//     } catch (err) {
//         console.error("Error updating review", err);
//         res.status(500).send("Internal Server Error");
//     }
// });

// module.exports = router;



const express = require('express');
const router = express.Router();
const { MongoClient, ObjectId } = require('mongodb'); // Corrected here

// Update a review
router.put('/:id', async (req, res) => {
    const reviewId = req.params.id;
    const updatedData = req.body;

    try {
        const client = await MongoClient.connect('mongodb://localhost:27017');
        const db = client.db("tour_travel");

        // Update the review
        const reviewResult = await db.collection('reviews').updateOne(
            { _id: new ObjectId(reviewId) }, // Corrected here
            { $set: updatedData } 
        );

        if (reviewResult.matchedCount === 0) {
            return res.status(404).send("Review not found");
        }

        // Retrieve the updated review to get the tourId
        const updatedReview = await db.collection('reviews').findOne({ _id: new ObjectId(reviewId) });
        const tourId = updatedReview.tourId;

        if (tourId) {
            // Ensure the tour document has this review in its reviews array
            await db.collection('tourdetails').updateOne(
                { _id: new ObjectId(tourId) }, // Corrected here
                { $addToSet: { reviews: reviewId } }
            );
        }

        res.status(200).send("Review updated successfully");
        client.close();  // Close the database connection
    } catch (err) {
        console.error("Error updating review", err);
        res.status(500).send("Internal Server Error");
    }
});

module.exports = router;
