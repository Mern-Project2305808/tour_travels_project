const express = require('express')
const router =express.Router();
const mongodb = require('mongodb').MongoClient;
const bcrypt = require('bcrypt');//for hashing passwords

module.exports = router.post('/', async(req,res) => {
    const { mail, password } =req.body;

    try {
        const client = await mongodb.connect('mongodb://localhost:27017' );
        const db = client.db('Registration');
        const user = await db.collection('users').findOne({ mail: mail});

        if (!user){
            res.status(400).send("user is not registered");
        }

        const isPasswordMatch =  await bcrypt.compare(password , user.password);
        if(!isPasswordMatch){
            return res.status(400).send("password Invalid");
        }

        res.status(200).send("Login Successfully");
        client.close();
    }catch (err) {
        console.error("Error during login" , err);
        res.status(500).send("Internal server error")
    }
})