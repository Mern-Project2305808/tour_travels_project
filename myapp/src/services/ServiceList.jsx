import React from 'react'
import ServiceCard from './ServiceCard';
import {Col} from "reactstrap";

import weatherImg from '../assets/images/weather.png'
import guideImg from '../assets/images/guide.png'
import customizationImg from '../assets/images/customization.png'

const servicesData = [
   {
    imgUrl: weatherImg,
    title: "calculate weather",

    desc: "We follows varoius Weather safety preacautions So, Remember to stay updated on weather conditions to ensure a smooth and enjoyable trip",

    desc: "Lorem ipsum dolor sit amet, consectetur adipisicing elit.",

   },
   {
    imgUrl: guideImg,
    title: "Best Tour Guide ",

    desc: "That’s great to hear! As a travel agency, providing top-notch services is crucial. Remember to highlight your unique selling points, such as personalized itineraries, knowledgeable guides, and hassle-free experiences. Safe travels!.",

    desc: "Lorem ipsum dolor sit amet, consectetur adipisicing elit.",

   },
   {
    imgUrl: customizationImg,
    title: "Customization",

    desc: "Providing top-notch services is crucial.  your commitment to excellence will surely attract more travelers. Remember to highlight your unique selling points, such as personalized itineraries, knowledgeable guides",

    
   },
]

const ServiceList = () => {
  return (
    <>
   {servicesData.map((item,index) => (
    <Col lg="3" key={index}>
      <ServiceCard item={item} />
    </Col>
   ))}
   </>
  )
}


export default ServiceList

