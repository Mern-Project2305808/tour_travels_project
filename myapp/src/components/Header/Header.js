// import React,{useRef, useEffect}  from 'react'
// import { Container, Row,Button} from 'reactstrap'
// import { NavLink, Link } from 'react-router-dom'
// import logo from '../../assets/images/logo.png'
// import './header.css'
// // import {isActive} from 'react-router-dom'

// const nav_links=[
//     {
//         path:'/home',
//         display:'Home'
//     },
//     {
//         path:'/about',
//         display:'About'
//      },
//      {
//         path:'/tours',
//         display:'Tour'
//      },
// ]

// const Header = () => {

//   const headerRef = useRef(null)

//   const stickyHeaderFunc =()=>{
//     window.addEventListener('scroll', ()=>{
//       if(document.body.scrollTop >80 || document.documentElement.scrollTop >80){
//         headerRef.current.classList.add('sticky__header')
//       } else {
//         headerRef.current.classList.remove("sticky__header");
//       }
//     })

//   }

//   useEffect(() => {
//     stickyHeaderFunc();
//     return window.removeEventListener("scroll",stickyHeaderFunc);
//   });

//   return (
    
//         <header className='header' ref={headerRef}>
//            <Container>
//             <Row>
//                 <div className="nav__wrapper d-flex align-items-center justify-content-between">
//                   {/*___________logo________ */}
//                   <div className='logo'>
//                       <img src={logo} alt=''/>
//                   </div>
//                   {/*________logo end ___________*/}
//                   {/*_________menu start _____*/}
//                   <div className="navigation">
//                     <ul className='menu d-flex align-items-center gap-5'> 
//                         {nav_links.map((item, index)=>(
//                             <li className='nav__item' key={index}>
//                                 <NavLink to={item.path} className={({ isActive }) => isActive ? 'active__link' : ''}>
//                                     {item.display}
//                                 </NavLink>
//                             </li>
//                         ))}
//                     </ul>
//                   </div>
//                   {/*___________menu end _________*/}
                 
//                  <div className="nav__right d-flex align-items-center gap-4 ">
//                    <div className="nav__btns d-flex align-items-center gap-4">
//                      <Button className="btn secondary__btn">
//                       <Link to='/login'>Login</Link>
//                      </Button>
//                      <Button className="btn primary__btn">
//                       <Link to='/register'>Register</Link>
//                      </Button>
//                    </div>
//                    <span className='mobile__menu'>
//                    <i class="ri-menu-line"></i>
//                    </span>
//                  </div>
//                  </div>
 
//             </Row>
//            </Container>
//         </header>
//     // </div>
//   )
// }

// export default Header

import React, { useRef, useEffect, useState } from 'react';
import { Container, Row, Button } from 'reactstrap';
import { NavLink, Link, useNavigate } from 'react-router-dom';
import logo from '../../assets/images/logo.png';
import './header.css';

const nav_links = [
  {
    path: '/home',
    display: 'Home'
  },
  {
    path: '/about',
    display: 'About'
  },
  {
    path: '/tours',
    display: 'Tour'
  }
];

const Header = () => {
  const headerRef = useRef(null);
  const [isLoggedIn, setIsLoggedIn] = useState(false);
  const navigate = useNavigate();

  const stickyHeaderFunc = () => {
    window.addEventListener('scroll', () => {
      if (document.body.scrollTop > 80 || document.documentElement.scrollTop > 80) {
        headerRef.current.classList.add('sticky__header');
      } else {
        headerRef.current.classList.remove('sticky__header');
      }
    });
  };

  useEffect(() => {
    stickyHeaderFunc();
    return () => window.removeEventListener('scroll', stickyHeaderFunc);
  });

  useEffect(() => {
    const loginStatus = localStorage.getItem('isLoggedIn');
    if (loginStatus === 'true') {
      setIsLoggedIn(true);
    }
  }, []);

  const handleLogout = () => {
    localStorage.removeItem('isLoggedIn');
    localStorage.removeItem('email');
    setIsLoggedIn(false);
    navigate('/login');
  };

  return (
    <header className='header' ref={headerRef}>
      <Container>
        <Row>
          <div className="nav__wrapper d-flex align-items-center justify-content-between">
            <div className='logo'>
              <img src={logo} alt='Logo' />
            </div>
            <div className="navigation">
              <ul className='menu d-flex align-items-center gap-5'>
                {nav_links.map((item, index) => (
                  <li className='nav__item' key={index}>
                    <NavLink to={item.path} className={({ isActive }) => isActive ? 'active__link' : ''}>
                      {item.display}
                    </NavLink>
                  </li>
                ))}
              </ul>
            </div>
            <div className="nav__right d-flex align-items-center gap-4">
              <div className="nav__btns d-flex align-items-center gap-4">
                {isLoggedIn ? (
                  <Button className="btn secondary__btn" onClick={handleLogout}>
                    Logout
                  </Button>
                ) : (
                  <>
                    <Button className="btn secondary__btn">
                      <Link to='/login'>Login</Link>
                    </Button>
                    <Button className="btn primary__btn">
                      <Link to='/register'>Register</Link>
                    </Button>
                  </>
                )}
              </div>
              <span className='mobile__menu'>
                <i className="ri-menu-line"></i>
              </span>
            </div>
          </div>
        </Row>
      </Container>
    </header>
  );
};

export default Header;

