import React from 'react'
import Slider from 'react-slick'
import ava01  from '../../assets/images/ava-1.jpg'
import ava02  from '../../assets/images/ava-2.jpg'
import ava03  from '../../assets/images/ava-3.jpg'



const Testimonials = () => {
    const settings= {
        dots:true,
        infinite:true,
        autoplay:true,
        speed:1000,
        swipeToSlide:true,
        autoplaySpeed:2000,
        slidesToShow:3,

        responsive:[
            {
                breakpoint:992,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    infinite: true,
                    dots:true,
                },
            },
            {
                breakpoint:576,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    infinite: true,
                    dots:true,
                },
            },
        ]
    }
  return (
    <Slider {...settings}>
       <div className='testimonial py-4 px-3'>
        <p>
        Their work on our website and Internet marketing has made a significant different to our business. We’ve seen a 425% increase in quote requests from the website which has been pretty remarkable – but I’d always like to see more!
        </p>
        <div className='d-flex align-items-center gap-4 mt-3'>
            <img src={ava01} className='w-25 h-25 rounded-2' alt=""/>
            <div>
                <h5 className='mb-0 mt-3'>Praveen</h5>
                <p>Customer</p>
            </div>
        </div>
       </div> 
       <div className='testimonial py-4 px-3'>
        <p>
        Our tour guide from day one was amazing—caring and incredibly knowledgeable. The actual tour was equally incredible. Thanks to everyone who made this trip unforgettable!” 
        </p>
        <div className='d-flex align-items-center gap-4 mt-3'>
            <img src={ava02} className='w-25 h-25 rounded-2' alt=""/>
            <div>
                <h5 className='mb-0 mt-3'>Franklin</h5>
                <p>Customer</p>
            </div>
        </div>
       </div> 
       <div className='testimonial py-4 px-3'>
        <p>
        I had a fantastic experience with (TA). Their customer service agent, (AN), helped me find the best deal for my flight. The way she provided all the information made booking easy. I highly recommend (TA) to other travelers
        </p>
        <div className='d-flex align-items-center gap-4 mt-3'>
            <img src={ava03} className='w-25 h-25 rounded-2' alt=""/>
            <div>
                <h5 className='mb-0 mt-3'>Kumar</h5>
                <p>Customer</p>
            </div>
        </div>
       </div> 
    </Slider>
  )
}

export default Testimonials
