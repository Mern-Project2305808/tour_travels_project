import React from 'react'
import '../styles/home.css'
import { Container,Row,Col } from 'reactstrap';
import heroImg from '../assets/images/hero-img01.jpg'
import heroImg02 from '../assets/images/hero-img02.jpg'
import heroVideo from '../assets/images/hero-video.mp4'
import worldImg from '../assets/images/world.png'
import Subtitle from '../shared/Subtitle';
 import SearchBar from '../shared/SearchBar';
 import ServiceList from '../services/ServiceList';
 import FeaturedTourList from '../components/Featured-tours/FeaturedTourList';
 import experienceImg from '../assets/images/experience.png'
import MasonryImagesGallery from '../image-gallery/MasonryImagesGallery';
import Testimonials from '../components/testimonial/Testimonials'
import Newsletter from '../shared/Newsletter';


const Home = () => {
  // const isLoggedIn = localStorage.getItem('isLoggedIn') === 'true';

  return (
  
   <>

   {/* ___________ hero section start__________ */}
   <section>
    <Container>
      <Row>
        <Col>
        <div className='hero__content'>
           <div className='hero__subtitle d-flex align-items-center '>
             <Subtitle subtitle={'Know Before You Go'}/>
             {/* <img src={worldImg} alt='worldimg' /> */}
           </div>
           <h1>
            Traveling opens the door to creating{" "}
            <span className='highlight'>memories</span>
           </h1>
           <p>
           The world is a book, and those who do not travel read only a page

 Take only memories, leave only footprints.
           </p>
        </div>
        </Col>{/* ___________ hero section start__________ */}

        <Col lg='2'>
          <div className='hero__img-box'>
            <img src={heroImg} alt='' />
          </div>
        </Col>
        <Col lg='2'>
          <div className='hero__img-box mt-4'>
            <video src={heroVideo} alt=''  controls/>
          </div>
        </Col>
        <Col lg='2'>
          <div className='hero__img-box mt-5'>
            <img src={heroImg02} alt=''  />
          </div>
        </Col>
        <SearchBar />
      </Row>
    </Container>
   </section>
   {/* ___________ hero section end__________ */}
   <section>
    <Container>
      <Row>
        <Col lg='3'>
          <h5 className='services__subtitle'>What we serve</h5>
          <h2 className='services__title'>We offer our best services</h2>
        </Col>
        <ServiceList />
      </Row>
    </Container>
   </section>


   {/*___________featured tour section start ________*/}
   <section>
    <Container>
      <Row>
        <Col lg='12' className='mb-5'>
          <Subtitle Subtitle={'Explore'} />
           <h2 className='featured__tour-title'>Our featured tours</h2>
        </Col>
        <FeaturedTourList />
      </Row>
    </Container>
   </section>
   {/*___________featured tour section end ________*/}


    {/*___________experience section start ________*/}

    <section>
      <Container>
        <Row>
          <Col lg="6" >
              <div className='experience__content'>
                <Subtitle  Subtitle={"Experience"} />

                <h2>
                  with our all experience <br /> we will serve you
                </h2>
                <p>
                  We are here to serve you better, and we want you to know that your happiness is our top priority.
                  <br /> 
                
                </p>
              </div>
              <div className='counter__wrapper d-flex align-items-center gap-5'>
                <div className='counter__box'>
                  <span>12k+</span>
                  <h6>Successful trip</h6>
                </div>
                <div className='counter__box'>
                  <span>2k+</span>
                  <h6>Regular clients</h6>
                </div>
                <div className='counter__box'>
                  <span>15</span>
                  <h6>Years experience</h6>
                </div>
              </div>
          </Col>
          <Col lg="6">
          <div className='experience__img'>
            <img src={experienceImg} alt='' />
          </div>
          </Col>
        </Row>
      </Container>
    </section>

     {/*___________experience section end ________*/}


      {/*___________gallery section start __________*/}
      <section>
        <Container>
          <Row>
            <Col lg='12'>
              <Subtitle Subtitle={'Gallery'} />
              <h2 className='gallery__title'>Visit our customers tour gallery</h2>
            </Col>
            <Col lg='12' >
              <MasonryImagesGallery />
            </Col>
          </Row>
        </Container>
      </section>
      {/* const handleSlideClick = (route) =>{
      if (isLoggedIn) {
        navigate(route);
      } else {
        navigate('/login');
      }
    }; */}
      {/*___________gallery section  end ________*/}

      {/*___________testimonial section  start ________*/}
       
      <section>
        <Container>
          <Row>
            <Col lg='12'>
              <Subtitle Subtitle={'Fans Love'} />
              <h2 className='testimonial__title'>What our fans say about us </h2>
            </Col>
            <Col lg='12' >
              <Testimonials />
            </Col>
          </Row>
        </Container>
      </section>

       {/*___________testimonial section  end ________*/}
      <Newsletter />
   </>
  )
}

export default Home
