// import React ,{useState} from 'react';
// import CommonSection from './../shared/CommonSection'
//  import { Container, Col, Row } from 'reactstrap';
//  import { useLocation } from 'react-router-dom';
//  import TourCard from './../shared/TourCard'

//  const SearchResultList = () => {

//   const location = useLocation();
//   const [data] = useState(location.state);
//   console.log(data)


//   return (
//     <>
//     <CommonSection title={"Tour Search Result"} />
//     <section>
//            <Container>
//        <Row>
          
//         </Row>
//      </Container>
//    </section>    
//  </>
//    );
// }
// export default SearchResultList

import React, { useState } from 'react';
import CommonSection from './../shared/CommonSection';
import { Container, Col, Row } from 'reactstrap';
import { useLocation } from 'react-router-dom';
import TourCard from './../shared/TourCard';

const SearchResultList = () => {
    const location = useLocation();
    const [data, setData] = useState(location.state || []); // Default to an empty array if state is undefined
    
    console.log(data); // Check the received data

    return (
        <>
            <CommonSection title={"Tour Search Result"} />
            <section>
                <Container>
                    <Row>
                        {data && data.length > 0 ? (
                            data.map((tour) => (
                                <Col lg="3" key={tour._id}>
                                    <TourCard tour={tour} />
                                </Col>
                            ))
                        ) : (
                            <h4>No tours found</h4>
                        )}
                    </Row>
                </Container>
            </section>
        </>
    );
};

export default SearchResultList;
