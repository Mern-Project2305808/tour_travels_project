import React from 'react';
import '../styles/about.css';

const AboutPage = () => {
  return (
    <div className="about-container">
      <div className="about-content">
        <h1>About Us</h1>
        <p>
          Welcome to our website! We are committed to providing the best service possible. Our team is dedicated and passionate about our work, and we strive to meet and exceed our customers' expectations.
        </p>
        <h2>Our Mission</h2>
        <p>
          Our mission is to deliver high-quality products and services that improve the lives of our customers. We believe in innovation, integrity, and excellence in everything we do.
        </p>
        <h2>Our Team</h2>
        <p>
          Our team is composed of skilled professionals with diverse backgrounds and expertise. We work together to achieve our common goals and ensure the success of our clients.
        </p>
        {/* <h2>Contact Us</h2>
        <p>
          If you have any questions or need further information, please do not hesitate to contact us. We are here to help you.
        </p> */}
      </div>
    </div>
  );
};

export default AboutPage;
