// import React, { useState } from "react";
// import { Container, Row, Col, Form, FormGroup, Button, Alert } from 'reactstrap';
// import { Link } from 'react-router-dom';
// import axios from 'axios';
// import '../styles/login.css';

// import registerImg from '../assets/images/register.png';
// import userIcon from '../assets/images/user.png';

// const Register = () => {
//   const [credentials, setCredentials] = useState({
//     username: '',
//     mail: '',
//     password: ''
//   });

//   const [message, setMessage] = useState('');

//   const handleChange = e => {
//     setCredentials(prev => ({ ...prev, [e.target.id]: e.target.value }));
//   };

//   const handleSubmit = async e => {
//     e.preventDefault();
//     try {
//       const response = await axios.post('http://localhost:5000/register', {
//         username: credentials.username,
//         mail: credentials.mail,
//         password: credentials.password
//       });
//       console.log(response.data);
//       setMessage("Register Successfully");
//       handleReset(); // Reset the form after a successful registration
//     } catch (error) {
//       console.error("There was an error registering!", error);
//       setMessage("Register not Successful");
//       handleReset(); // Reset the form even if there is an error
//     }
//   };

//   const handleReset = () => {
//     setCredentials({
//       username: '',
//       mail: '',
//       password: ''
//     });
//   };

//   return (
//     <section>
//       <Container>
//         <Row>
//           <Col lg='8' className="m-auto">
//             <div className="login__container d-flex justify-content-between">
//               <div className="login__img">
//                 <img src={registerImg} alt="" />
//               </div>
//               <div className="login__form">
//                 <div className="user">
//                   <img src={userIcon} alt="" />
//                 </div>
//                 <h2>Register</h2>
//                 {message && <Alert color="info">{message}</Alert>}
//                 <Form onSubmit={handleSubmit}>
//                   <FormGroup>
//                     <input
//                       type="text"
//                       placeholder="Username"
//                       required
//                       id="username"
//                       value={credentials.username}
//                       onChange={handleChange}
//                     />
//                   </FormGroup>
//                   <FormGroup>
//                     <input
//                       type="text"
//                       placeholder="Mail Id"
//                       required
//                       id="mail"
//                       value={credentials.mail}
//                       onChange={handleChange}
//                     />
//                   </FormGroup>
//                   <FormGroup>
//                     <input
//                       type="password"
//                       placeholder="Password"
//                       required
//                       id="password"
//                       value={credentials.password}
//                       onChange={handleChange}
//                     />
//                   </FormGroup>
//                   <Button className="btn secondary__btn auth__btn" type="submit">
//                     Create Account
//                   </Button>
//                 </Form>
//                 <p>Already have an account? <Link to='/login'>Login</Link></p>
//               </div>
//             </div>
//           </Col>
//         </Row>
//       </Container>
//     </section>
//   );
// };

// export default Register;

import React, { useState, useEffect } from 'react';
import { NavLink, useNavigate } from 'react-router-dom';
import axios from 'axios';
import registerImg from '../assets/images/register.png';
 import userIcon from '../assets/images/user.png';
import '../styles/login.css'; // Ensure the CSS file is correctly imported

const url = 'http://localhost:5000/register';
const getUsersUrl = 'http://localhost:5000/fetch';


function Register() {
  const [name, setName] = useState('');
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const [email, setEmail] = useState('');
  const [usersLength, setUsersLength] = useState(0);
  const [error, setError] = useState('');

  useEffect(() => {
    const fetchUsersLength = async () => {
      try {
        const response = await axios.get(getUsersUrl);
        setUsersLength(response.data.length);
      } catch (error) {
        console.error('Error fetching users:', error);
      }
    };

    fetchUsersLength();
  }, []);

  const navigate = useNavigate();

  const handleIncrease = async (e) => {
    e.preventDefault();
    if (password !== confirmPassword) {
      setError('Passwords do not match');
      return;
    }
    const newUser = { id: usersLength, name, password, email };
    try {
      const response = await axios.post(url, newUser);
      console.log(response.data);
      handleReset();
      navigate('/login');
    } catch (error) {
      console.error('Error registering new user:', error);
      if (error.response && error.response.status === 400) {
        setError('Email already exists');
      } else {
        setError('An error occurred while registering. Please try again later.');
      }
    }
  };

  const handleReset = () => {
    setName('');
    setEmail('');
    setPassword('');
    setConfirmPassword('');
    setError('');
  };

  return (
    <section>
      <div className="container">
        <div className="row">
          <div className="col-lg-8 m-auto">
            <div className="login__container d-flex justify-content-between">
              <div className="login__img">
                <img src={registerImg} alt="" /> {/* Update with the correct path to your image */}
              </div>
              <div className="login__form">
                <div className="user">
                  <img src={userIcon} alt="" /> {/* Update with the correct path to your icon */}
                </div>
                <h2>Register</h2>
                {error && <p style={{ color: 'red' }}>{error}</p>}
                <form onSubmit={handleIncrease} onReset={handleReset}>
                  <input
                    type="text"
                    placeholder="Name"
                    value={name}
                    onChange={(e) => setName(e.target.value)}
                    required
                  />
                  <input
                    type="email"
                    placeholder="E-mail"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                    required
                  />
                  <input
                    type="password"
                    placeholder="Password"
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                    required
                  />
                  <input
                    type="password"
                    placeholder="Confirm Password"
                    value={confirmPassword}
                    onChange={(e) => setConfirmPassword(e.target.value)}
                    required
                  />
                  <button className="btn secondary__btn auth__btn" type="submit">
                    Signup
                  </button>
                  <button className="btn secondary__btn auth__btn" type="reset">
                    Reset
                  </button>
                </form>
                <p>Already have an account? <NavLink to='/login'>Login</NavLink></p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}

export default Register;
