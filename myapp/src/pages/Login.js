
// import React, { useState } from "react";
// import { Container, Row, Col, Form, FormGroup, Button, Alert } from 'reactstrap';
// import { Link } from 'react-router-dom';
// import '../styles/login.css';
// import loginImg from '../assets/images/login.png';
// import userIcon from '../assets/images/user.png';
// import axios from 'axios';

// const Login = () => {
//   const [credentials, setCredentials] = useState({
//     mail: '',
//     password: ''
//   });

//   const [message, setMessage] = useState('');

//   const handleSubmit = async e => {
//     e.preventDefault();
//     try {
//       const response = await axios.post('http://localhost:5000/login', {
//         mail: credentials.mail,
//         password: credentials.password
//       });
//       console.log(response.data);
//       setMessage("Login Successfully");
//       handleReset(); // Reset the form after a successful login
//       // Handle success response here (e.g., show a success message, redirect to another page)
//     } catch (error) {
//       console.error("There was an error logging in!", error);
//       if (error.response && error.response.data) {
//         setMessage(error.response.data.message);
//       } else {
//         setMessage("Login not Successful");
//       }
//       handleReset(); // Reset the form even if there is an error
//       // Handle error response here (e.g., show an error message)
//     }
//   };

//   const handleReset = () => {
//     setCredentials({
//       mail: '',
//       password: ''
//     });
//   };

//   const handleChange = e => {
//     setCredentials(prev => ({ ...prev, [e.target.id]: e.target.value }));
//   };

//   return (
//     <section>
//       <Container>
//         <Row>
//           <Col lg='8' className="m-auto">
//             <div className="login__container d-flex justify-content-between">
//               <div className="login__img">
//                 <img src={loginImg} alt="" />
//               </div>
//               <div className="login__form">
//                 <div className="user">
//                   <img src={userIcon} alt="" />
//                 </div>
//                 <h2>Login</h2>
//                 {message && <Alert color="info">{message}</Alert>}
//                 <Form onSubmit={handleSubmit}>
//                   <FormGroup>
//                     <input
//                       type="text"
//                       placeholder="Enter Your Mail Id"
//                       required
//                       id="mail"
//                       value={credentials.mail}
//                       onChange={handleChange}
//                     />
//                   </FormGroup>
//                   <FormGroup>
//                     <input
//                       type="password"
//                       placeholder="Enter Your Password"
//                       required
//                       id="password"
//                       value={credentials.password}
//                       onChange={handleChange}
//                     />
//                   </FormGroup>
//                   <Button className="btn secondary__btn auth__btn" type="submit">
//                     Login
//                   </Button>
//                 </Form>
//                 <p>Don't have an account? <Link to='/register'>Create</Link></p>
//               </div>
//             </div>
//           </Col>
//         </Row>
//       </Container>
//     </section>
//   );
// };

// export default Login;

// import React, { useRef, useState } from 'react';
// import { NavLink, useNavigate } from 'react-router-dom';
// import axios from 'axios';
// import loginImg from '../assets/images/login.png';
// import userIcon from '../assets/images/user.png';
// import '../styles/login.css';

// const loginUrl = 'http://localhost:5000/api';

// function Login() {
//   const emailRef = useRef(null);
//   const pwdRef = useRef(null);
//   const navigate = useNavigate();
//   const [error, setError] = useState('');

//   const handleSubmit = async (e) => {
//     e.preventDefault();
//     const email = emailRef.current.value;
//     const password = pwdRef.current.value;

//     try {
//       const response = await axios.post(loginUrl, { email, password, login: true });

//       if (response.status === 200) {
//         setError('');
//         localStorage.setItem('email', email);
//         localStorage.setItem('isLoggedIn', 'true');
//         navigate('/');
//       } else {
//         setError('Invalid credentials. Please try again.');
//       }
//     } catch (error) {
//       console.error('Error during login:', error);
//       setError('An error occurred while authenticating. Please try again later.');
//     }
//   };

//   return (
//     <section>
//       <div className="container">
//         <div className="row">
//           <div className="col-lg-8 m-auto">
//             <div className="login__container d-flex justify-content-between">
//               <div className="login__img">
//                 <img src={loginImg} alt="" /> {/* Update with the correct path to your image */}
//               </div>
//               <div className="login__form">
//                 <div className="user">
//                   <img src={userIcon} alt="" /> {/* Update with the correct path to your icon */}
//                 </div>
//                 <h2>Login</h2>
//                 {error && <p style={{ color: 'red' }}>{error}</p>}
//                 <form onSubmit={handleSubmit}>
//                   <input
//                     type="text"
//                     placeholder="Email"
//                     required
//                     ref={emailRef}
//                   />
//                   <input
//                     type="password"
//                     placeholder="Password"
//                     required
//                     ref={pwdRef}
//                   />
//                   <br /><br />
//                   <button className="btn secondary__btn auth__btn" type="submit">
//                     Login
//                   </button>
//                 </form>
//                 <p>Don't have an account? <NavLink to='/register'>Create</NavLink></p>
//               </div>
//             </div>
//           </div>
//         </div>
//       </div>
//     </section>
//   );
// }

// export default Login;


import React, { useRef, useState } from 'react';
import { NavLink, useNavigate } from 'react-router-dom';
import axios from 'axios';
import loginImg from '../assets/images/login.png';
import userIcon from '../assets/images/user.png';
import '../styles/login.css';

const loginUrl = 'http://localhost:5000/api';

function Login() {
  const emailRef = useRef(null);
  const pwdRef = useRef(null);
  const navigate = useNavigate();
  const [error, setError] = useState('');

  const handleSubmit = async (e) => {
    e.preventDefault();
    const email = emailRef.current.value;
    const password = pwdRef.current.value;

    try {
      const response = await axios.post(loginUrl, { email, password, login: true });

      if (response.status === 200) {
        setError('');
        localStorage.setItem('email', email);
        localStorage.setItem('isLoggedIn', 'true');
        navigate('/');
      } else {
        setError('Invalid credentials. Please try again.');
      }
    } catch (error) {
      console.error('Error during login:', error);
      setError('An error occurred while authenticating. Please try again later.');
    }
  };

  const handleReset = () => {
    emailRef.current.value = '';
    pwdRef.current.value = '';
    setError('');
  };

  return (
    <section>
      <div className="container">
        <div className="row">
          <div className="col-lg-8 m-auto">
            <div className="login__container d-flex justify-content-between">
              <div className="login__img">
                <img src={loginImg} alt="Login" />
              </div>
              <div className="login__form">
                <div className="user">
                  <img src={userIcon} alt="User Icon" />
                </div>
                <h2>Login</h2>
                {error && <p style={{ color: 'red' }}>{error}</p>}
                <form onSubmit={handleSubmit}>
                  <input
                    type="text"
                    placeholder="Email"
                    required
                    ref={emailRef}
                  />
                  <input
                    type="password"
                    placeholder="Password"
                    required
                    ref={pwdRef}
                  />
                  <br /><br />
                  <button className="btn secondary__btn auth__btn" type="submit">
                    Login
                  </button>
                  <button className="btn secondary__btn auth__btn" type="button" onClick={handleReset}>
                    Reset
                  </button>
                </form>
                <p>Don't have an account? <NavLink to='/register'>Create</NavLink></p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}

export default Login;
