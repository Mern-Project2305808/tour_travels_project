// const express = require('express')
// const router = express.Router();
// const mongodb = require('mongodb').MongoClient;
// const ObjectId = require('mongodb').ObjectId; 

// module.exports = router.delete('/:id', (req, res) => {
//     const dataId = req.params.id;

//     mongodb.connect('mongodb://localhost:27017/tour_travel', (err, db) => {
//         if(err) {
//             console.error("error connecting to mongodb", err);
//             return res.status(500).send("Internal Server Error");
//         }
//         db.connect('tourdetails').deleteOne({ _id: new ObjectId(dataId) }, (err, result) => {
//             if(err) {
//                 console.error("error deleting data from collection", err);
//                 return res.status(500).send("Internal Server Error");
//             }
//             else{
//                 if(result.length === 0){
//                     res.status(404).send("data not found");
//                 }
//                 else{
//                     res.status(200).send("data deleted")
//                 }
//             }
//         })
//     })
// })






const express = require('express');
const router = express.Router();
const mongodb = require('mongodb').MongoClient;
const ObjectId = require('mongodb').ObjectId; // Import ObjectId

router.delete('/:id', (req, res) => {
    const dataId = req.params.id;

    mongodb.connect('mongodb://localhost:27017/tour_travel', { useUnifiedTopology: true }, (err, client) => {
        if (err) {
            console.error("Error connecting to MongoDB", err);
            return res.status(500).send("Internal Server Error");
        }

        const db = client.db('tour_travel'); // Get the database
        const collection = db.collection('tourdetails'); // Get the collection

        collection.deleteOne({ _id: new ObjectId(dataId) }, (err, result) => {
            client.close(); // Close the connection after the operation

            if (err) {
                console.error("Error deleting data from collection", err);
                return res.status(500).send("Internal Server Error");
            }

            if (result.deletedCount === 0) {
                return res.status(404).send("Data not found");
            }

            res.status(200).send("Data deleted");
        });
    });
});

module.exports = router;
