const express = require('express');
const fs = require('fs')
const app = express();
const PORT = 5000;
const cors = require('cors')
const corsOptions = {
    origin: 'http://localhost:3000',  // Adjust the origin to match your frontend's URL
    optionsSuccessStatus: 200
};
app.use(cors(corsOptions));




// app.use(cors());
app.use(express.json());
app.use('/insert',require('./insert'))
app.use('/fetch',require('./fetch'))
app.use('/update',require('./update'))
app.use('/delete', require('./delete'))
app.use('/search', require('./getTourBySearch'));
app.use('/search/getFeaturedTour',require('./getFeaturedTours'))
app.use('/register',require('./register'))
app.use('/login',require('./login'))
app.use('/update_review', require('./reviews'))
app.use('/insert_review',require('./reviewinsert'))
app.use('/fetch/update_review', require('./update_review'))
app.use('/booking_insert' , require('./booking_insert'))
app.use('/booking_update' , require('./booking_updated'))
app.use('/booking_fetch', require('./booking_fetch'))



app.listen(PORT,()=>{
    console.log(`server is running ${PORT}`)
})