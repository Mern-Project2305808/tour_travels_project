// const mongodb = require('mongodb')
// const bookingdata = db.createCollection('booking', {
//     validator: {
//         $jsonSchema: {
//             bsonType: "object",
//             required:["userId", "userEmail", "tourName", "fullName", "guestSize", "phone"],
//             properties: {
//                 userId: {
//                     bsonType:"string",
//                     description: "must ba a string and is required"
//                 },
//                 userEmail: {
//                     bsonType:"string",
//                     description: "must ba a string and is required"
//                 },
//                 tourName: {
//                     bsonType:"string",
//                     description: "must ba a string and is required"
//                 },
//                 fullName: {
//                     bsonType:"string",
//                     description: "must ba a string and is required"
//                 },
//                 guestSiz: {
//                     bsonType:"int",
//                     description: "must ba a int and is required"
//                 },
//                 phone:{
//                     bsonType:"string",
//                     description: "must ba a string and is required"
//                 }
//             }
//         }
//     },
//     validationLevel: "strict",  // All documents must comply with the schema
//     validationAction: "error"  //Rejects non-compliant documents

// })



const { MongoClient } = require('mongodb');

const mongoUri = 'mongodb://localhost:27017/tour_travel';

const createBookingCollection = async () => {
    try {
        const client = await MongoClient.connect(mongoUri);
        const db = client.db('tour_travel');
        
        const bookingdata = await db.createCollection('booking', {
            validator: {
                $jsonSchema: {
                    bsonType: "object",
                    required: ["userId", "userEmail", "tourName", "fullName", "guestSize", "phone"],
                    properties: {
                        userId: {
                            bsonType: "string",
                            description: "must be a string and is required"
                        },
                        userEmail: {
                            bsonType: "string",
                            description: "must be a string and is required"
                        },
                        tourName: {
                            bsonType: "string",
                            description: "must be a string and is required"
                        },
                        fullName: {
                            bsonType: "string",
                            description: "must be a string and is required"
                        },
                        guestSize: {
                            bsonType: "int",
                            description: "must be an int and is required"
                        },
                        phone: {
                            bsonType: "string",
                            description: "must be a string and is required"
                        }
                    }
                }
            },
            validationLevel: "strict",
            validationAction: "error"
        });

        console.log('Booking collection created with schema validation');
        client.close();
    } catch (err) {
        console.error('Error creating collection:', err);
    }
};

createBookingCollection();
