const express = require('express');
const router = express.Router();
const mongodb = require('mongodb').MongoClient;

router.get('/', (req, res) => {
    
    // const featured = req.query.FeaturedTour;

    mongodb.connect('mongodb://localhost:27017/tour_travel', (err, client) => {
        if (err) {
            console.error("mongodb connection error", err);
            return res.status(500).send("Server error");
        }

        const db = client.db('tour_travel');
        db.collection('tourdetails').find({
            featured:true
        }).toArray((err, data) => {
            if (err) {
                console.error("Error fetching data from collection", err);
                return res.status(500).send("Error fetching data");
            }

            if (data.length > 0) {
                res.status(200).json({
                    success: true,
                    message: "successful",
                    data: data,
                });
            } else {
                res.status(404).send("No tours found");
            }
        });
    });
});

module.exports = router;
