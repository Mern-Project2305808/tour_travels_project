const express = require('express')
const router = express.Router();
const mongodb = require('mongodb').MongoClient;
const  {booking} = require('./models/booking')

module.exports = router.post('/', (req, res) => {
    const {userId, userEmail, tourName, fullName, guestSize, phone} = req.body;

    mongodb.connect('mongodb://localhost:27017/tour_travel', (err, db) => {
        if(err) {
            console.error("mongodb connection error")
            return res.status(400).send("internal server error")
        }
        else{
            db.collection('booking').insertOne({ userId: userId , userEmail : userEmail, tourName: tourName, fullName: fullName, guestSize: guestSize, phone: phone}, (err, result) => {
                if(err) {
                    console.error(" data collection error")
                    return res.status(404).send("internal server error")
                }
                else{
                    console.log("data inserted")
                    return res.status(200).send(result)
                }
            })
        }
    })
})